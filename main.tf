data "google_client_config" "current" {}

resource "google_compute_network" "couchbase-capella-vpc-network" {
  project                 = var.project
  name                    = "couchbase-capella-vpc"
  auto_create_subnetworks = false
  mtu                     = 1460
}

# -----------------------------------------------------------------------------------
#  SUBNET
# -----------------------------------------------------------------------------------

resource "google_compute_subnetwork" "couchbase-capella-vpc-subnet-london" {
  name                     = "subnet-london"
  ip_cidr_range            = var.subnet-cidr
  region                   = var.gcp_region
  network                  = google_compute_network.couchbase-capella-vpc-network.name
  description              = "This is a custom subnet"
  private_ip_google_access = "true"

  secondary_ip_range {
    range_name    = "tf-test-secondary-range-update1"
    ip_cidr_range = "192.168.24.0/24"
  }
}

# -----------------------------------------------------------------------------------
# FIREWALL
# -----------------------------------------------------------------------------------
// web network tag

resource "google_compute_firewall" "couchbase-capella-vpc-network-allow-ssh" {
  project     = var.project
  name        = "allow-ssh"
  network     = google_compute_network.couchbase-capella-vpc-network.name
  description = "Creates firewall rule target"

  allow {
    protocol = "tcp"
    ports    = ["22"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["web-server"]
  timeouts {}

}

resource "google_compute_firewall" "couchbase-capella-vpc-network-allow-http" {
  project     = var.project
  name        = "allow-http"
  network     = google_compute_network.couchbase-capella-vpc-network.name
  description = "Creates firewall rule target"

  allow {
    protocol = "tcp"
    ports    = ["80"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["web-server"]
  timeouts {}

}

resource "google_compute_firewall" "couchbase-capella-vpc-network-allow-https" {
  project     = var.project
  name        = "allow-https"
  network     = google_compute_network.couchbase-capella-vpc-network.name
  description = "Creates firewall rule target"

  allow {
    protocol = "tcp"
    ports    = ["443"]
  }
  source_ranges = ["0.0.0.0/0"]
  target_tags   = ["web-server"]
  timeouts {}

}

resource "google_compute_instance" "couchbase-capella-vm-instance" {
  name         = var.instance-name
  hostname     = var.hostname
  project      = var.project
  zone         = var.zone
  machine_type = var.vm_type

  metadata = {
    ssh-key        = "${var.admin}:${file("./secret/plain.txt")}"
  
  }
  network_interface {
    network            = google_compute_network.couchbase-capella-vpc-network.self_link
    subnetwork         = google_compute_subnetwork.couchbase-capella-vpc-subnet-london.self_link
    subnetwork_project = data.google_client_config.current.project
    # network_ip         = var.private_ip
    access_config {
      //include this section to give the vm an external IP address
    }
  }

  tags = ["http", "https"]
  

  boot_disk {
    initialize_params {
      image = var.os_image[var.OS].name
    }
  }

  depends_on = [ google_compute_firewall.couchbase-capella-vpc-network-allow-ssh, google_compute_firewall.couchbase-capella-vpc-network-allow-http, google_compute_firewall.couchbase-capella-vpc-network-allow-https ]

  service_account {
    scopes = ["https://www.googleapis.com/auth/compute.readonly"]
  }
  
}