variable "project" {
  description = "Google Project ID"
  default = "cloud-375003"
}

variable "gcp_region" {
  description = "Google Cloud region"
  default     = "europe-west2"
}

variable "zone" {
  default = "europe-west2-a"
}

variable "main-cidr" {
  default = "192.0.0.0/16"
}

variable "subnet-cidr" {
  default = "192.168.0.0/24"
}

variable "vm_type" {
  default = "e2-standard-4" 
}

variable "hostname" {
  description = "Hostname of instance"
  default     = "couchbase.gcp-devfest.com"
}

variable "admin" {
  description = "OS user"
  default     = "ubuntu"
}

variable "osdisk_size" {
  default = "16"
}

variable "OS" { # gcloud compute image list --filter=name:ubuntu
  description = "the selected ami base os"
  default     = "UBUNTU"
}

variable "os_image" {
  default = {
    UBUNTU = {
      name = "ubuntu-os-cloud/ubuntu-2004-lts"
    }
  }
}

variable "instance-name" {
  default = "couchbase-capella-vm-instance"
}

# variable "private_ip" { # gcloud compute machine-types list --filter="zone:us-east1-b and name:e2-micro"
#   default = "10.10.12.0"
# }