terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "~> 3.75"
    }
  }
}

provider "google" {
  project = var.project
  region  = var.gcp_region
  zone    = var.zone
}